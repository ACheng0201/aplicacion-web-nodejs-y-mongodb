const mongoose = require ('mongoose');
const Schema = mongoose.Schema;

const Registro = new Schema({
    name: String,
    pay: String,
    status:{
        type:Boolean,
        default: false
    }
});

module.exports = mongoose.model('registros', Registro);