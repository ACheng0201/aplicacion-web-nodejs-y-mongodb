const express = require('express');
const router = express.Router();

const Registro = require('../models/registros');

router.get('/',async (req, res) => {
    const registros = await Registro.find();
    res.render('index', {
        registros
    });
});

router.post('/registrar',async (req, res)=>{
    const registro = new Registro(req.body);
    await registro.save();
    res.redirect('/');
});

router.get('/edit/:id', async (req, res) => {
    const {id} = req.params;
    const registro = await Registro.findById(id);
    res.render('edit',{
        registro
    });
});

router.post('/edit/:id', async (req, res)=> {
    const {id} = req.params;
    await Registro.update({_id:id}, req.body);
    res.redirect('/');
});

router.get('/delete/:id',async (req, res) => {
    const {id}= req.params;
   await Registro.remove({_id: id});
   res.redirect('/');
});

module.exports = router;
